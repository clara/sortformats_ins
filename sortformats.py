"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1: str, format2: str) -> bool:
    return fordered.index(format1) < fordered.index(format2)

def sort_pivot(formats: list, pivot: int) -> int:
    while pivot > 0 and lower_than(formats[pivot], formats[pivot - 1]):
        formats[pivot], formats[pivot - 1] = formats[pivot - 1], formats[pivot]
        pivot -= 1

def sort_formats(formats: list) -> list:
    for i in range(1, len(formats)):
        sort_pivot(formats, i)
    return formats

def main():
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()
